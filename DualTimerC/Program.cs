﻿

namespace DualTimerC
{
    using System;
    using DualTimer;
    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cro();
            bool agora=true;
            Console.WriteLine("Pressione Enter para iniciar o cronómetro");
            Console.ReadLine();
            relogio.Start();
            Console.WriteLine("Pressione Enter novamente para parar o cronómetro");

            while (agora)
            {
               
                Console.Write("\r Tempo Corrente: {0}", relogio.Contagem());
                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().Key == ConsoleKey.Enter)
                    {
                        agora=false;
                        break;
                    }
                }
            }
            Console.WriteLine("\r Tempo Cronometrado: {0}", relogio.Contagem());
            Console.ReadKey();
        }
    }
}
