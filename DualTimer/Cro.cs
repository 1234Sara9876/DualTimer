﻿

namespace DualTimer
{
    using System;
    public class Cro
    {
        #region atributos

        private DateTime iniciou;
        private TimeSpan decorreu;

        #endregion

        #region propriedades

        public DateTime Iniciou { get; set; }
        public TimeSpan Decorreu { get; set; }


        #endregion

        #region métodos construtores

        public Cro()
        {
            iniciou = DateTime.MinValue;
            decorreu = TimeSpan.Zero;
        } //construtor default

        public Cro(DateTime iniciou, TimeSpan decorreu)
        {
            Iniciou = iniciou;
            Decorreu = decorreu;
        }//construtor por parâmetros

        public Cro(Cro timex) : this(timex.Iniciou, timex.Decorreu) { }
        //construtor de cópia

        #endregion

        #region outros métodos

        public void Start() 
        {   
          iniciou = DateTime.Now;
        }

        public TimeSpan Contagem()
        {
            decorreu = DateTime.Now - iniciou;
            return decorreu;
        }

        public void Reset()
        {
            decorreu = TimeSpan.Zero;
        }

        #endregion

    }
}
