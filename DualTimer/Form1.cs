﻿
namespace DualTimer
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private readonly Cro tmx;
        public Form1()
        {
            InitializeComponent();
            tmx = new Cro();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Enabled = true;
        }

        private void timerT_Tick(object sender, EventArgs e)
        {
            label1.Text = tmx.Contagem().ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tmx.Start();
            timerT.Enabled = true;
            button1.Enabled = false;
            button2.Enabled = true;
            button3.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //tmx.Stop();
            timerT.Enabled = false;
            button1.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tmx.Reset();
            label1.Text = tmx.Decorreu.ToString();
            timerT.Enabled = false;
            button1.Enabled = true;
            button2.Enabled = false;
            button3.Enabled = false;

        }


    }
}
